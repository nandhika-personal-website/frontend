import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss';

export default function NanLogo() {
    return (
        <button id='l-nan-logo'>
            <Link to='/' id='c-nan-logo__link' className='h-animation-config' />
            <img id='c-nan-logo' className='h-animation-config' src='/media/icons/nan-logo.svg' alt='N Letter' />
            <div id='l-nan-logo__orbit' className='h-animation-config'>
                <div id='c-nan-logo__planet'></div>
            </div>
        </button>
    );
}