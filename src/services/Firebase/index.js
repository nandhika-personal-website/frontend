import React from 'react';

const config = {
	apiKey: "AIzaSyAW9LMeFEQDIDzSOnwwL3JfVL7HX09U3l8",
	authDomain: "personal-website-3cd11.firebaseapp.com",
	databaseURL: "https://personal-website-3cd11.firebaseio.com",
	projectId: "personal-website-3cd11",
	storageBucket: "personal-website-3cd11.appspot.com",
	messagingSenderId: "663434393512",
	appId: "1:663434393512:web:d479e5dad0185c0e"
};

export const FirebaseContext = React.createContext();
export default config;